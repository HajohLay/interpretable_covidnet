from matplotlib import image
import tensorflow as tf
import numpy as np
import cv2
import scipy as sp
import math
import os

def load_raw_image(image_path):
    img = cv2.imread(image_path)
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

def central_crop(img):
    size = min(img.shape[0], img.shape[1])
    offset_h = int((img.shape[0] - size)/2)
    offset_w = int((img.shape[1] - size)/2)
    return img[offset_h : offset_h + size, offset_w : offset_w + size]

def crop_top(img, percent=0.08):
    offset = int(img.shape[0] * percent)
    return img[offset:]

def load_cropped_image(image_path):
    #LOAD THE CROPPED IMAGE FOR INPUT TO NETWORK
    img = cv2.imread(image_path)
    img = crop_top(img)
    img = central_crop(img)
    img = cv2.resize(img, (480, 480))
    return img

class CovidNet():

    def __init__(self, model_path="flskdjf", target_name = 'conv4_block6_out/add:0'):
        self.sess = tf.Session()
        tf.get_default_graph()
        loader = tf.train.import_meta_graph(model_path + 'model.meta')
        loader.restore(self.sess, model_path + 'model')
        self.graph = tf.get_default_graph()
        # sess = tf.Session()
        # tf.get_default_graph()
        # saver = tf.train.import_meta_graph(model_path + "model.meta")
        # saver.restore(sess, model_path + 'checkpoint')

        # self.sess = sess
        # self.graph = tf.get_default_graph()

        # sess = tf.compat.v1.Session()
        # tf.compat.v1.keras.backend.set_session(sess)
        # init_op = tf.compat.v1.global_variables_initializer()
        # sess.run(init_op)
        # self.sess = sess
        # self.graph = sess.graph
        # self.model = tf.keras.models.load_model(model_path)

        # #tensors
        # self.image_tensor = self.graph.get_tensor_by_name("input_1:0")
        # self.target_tensor = self.graph.get_tensor_by_name(target_name)
        # self.out_tensor = self.graph.get_tensor_by_name('softmax/Softmax:0')
        self.image_tensor = self.graph.get_tensor_by_name("input_1:0")
        self.target_tensor = self.graph.get_tensor_by_name(target_name)
        self.out_tensor = self.graph.get_tensor_by_name('norm_dense_2/Softmax:0')
    
    def get_input_as_array(self,image_path):
        return load_cropped_image(image_path)
    
    # def get_softmax_layers(self):
    #     with open("covidnet_layer_names.txt", 'w') as f:
    #         names = [(n.name + '\n') for n in self.graph.get_operations()]
    #         f.writelines(names)
    #         # for name in names:
    #         #     if ('softmax/Softmax' in name):

     
    def get_prediction(self, image_dir):
        img = load_cropped_image(image_dir)/255.0
        results = self.sess.run([self.out_tensor], feed_dict={self.image_tensor:[img]})
        return results[0]
 
    def get_activations(self, image_dir):
        img = load_cropped_image(image_dir)/255.0
        activations = self.sess.run([self.target_tensor], feed_dict={self.image_tensor:[img]})
        max_activation = np.max(activations)
        return activations[0][0], max_activation
    
    def get_activations_for_dict(self, image_dir, no_features = 4):
        acts, max_act= self.get_activations(image_dir)
        cropped_acts = [[[{"n": n.tolist(), "v": float(act_vec[n])} for n in np.flip(np.argsort(act_vec)[-no_features:])] for act_vec in act_slice] for act_slice in acts]
        return cropped_acts, max_act
        
    def get_scores(self, image_dir, class_index = 0):
        img = load_cropped_image(image_dir)/255.0

        #compute grads
        one_hot = tf.sparse_to_dense(class_index, [2], 1.0)
        signal = tf.multiply(self.out_tensor, one_hot)
        loss = tf.reduce_mean(signal)
        grads = tf.gradients(loss, self.target_tensor)[0]
        
        #compute scores
        output, grads_val = self.sess.run([self.target_tensor, grads], feed_dict={self.image_tensor:[img]})

        # print(f'max act val is {np.max(output)}')
        # print(f'max grads val is {np.max(1000000000*grads_val)}')
        # print(f'min grads val is {np.min(grads_val)}')
        class_score = np.multiply(output, grads_val)[0]
        return class_score, np.max(class_score)
    
    def get_scores_for_dict(self, image_dir, class_index = 0, no_features = 4):
        scores, max_score = self.get_scores(image_dir, class_index=class_index)
        cropped_scores = [[[{"n": n.tolist(), "v": float(score_vec[n])} for n in np.flip(np.argsort(score_vec)[-no_features:])] for score_vec in score_slice] for score_slice in scores]
        return cropped_scores, max_score

    def get_smooth_scores(self, image_dir, class_index = 0):
        interpolated_images = self.interpolate_images(image_dir)

        #compute grads
        one_hot = tf.sparse_to_dense(class_index, [2], 1.0)
        signal = tf.multiply(self.out_tensor, one_hot)
        loss = tf.reduce_mean(signal)
        grads = tf.gradients(loss, self.target_tensor)[0]

        #output scores
        acts, grads_vals = self.sess.run([self.target_tensor, grads], feed_dict={self.image_tensor:interpolated_images})

        grads_by_vals = np.multiply(acts, grads_vals)
        smooth_scores = np.mean(grads_by_vals, axis=0)
        return smooth_scores, np.max(smooth_scores)
    
    def get_smooth_score_for_dict(self, image_dir, class_index = 0, no_features = 4):
        scores, max_score = self.get_smooth_scores(image_dir, class_index=class_index)
        cropped_scores = [[[{"n": n.tolist(), "v": float(score_vec[n])} for n in np.flip(np.argsort(score_vec)[-no_features:])] for score_vec in score_slice] for score_slice in scores]
        return cropped_scores, max_score 
    
    def interpolate_images(self, image_dir, no_interpolations = 5):
        img = load_cropped_image(image_dir)/255.0
        interpolated_images = []
        for i in range(1,no_interpolations+1):
            interpolated_images.append(img * (i/(no_interpolations)))
        return interpolated_images
    
    def get_integrated_image_gradients(self, image_dir, class_index = 0, no_interpolations = 5,):
        interpolated_images = self.interpolate_images(image_dir, no_interpolations=no_interpolations)
        #get activations for batch 
        one_hot = tf.sparse_to_dense(class_index, [2], 1.0)
        signal = tf.multiply(self.out_tensor, one_hot)
        loss = tf.reduce_mean(signal)
        grads = tf.gradients(loss, self.image_tensor)[0]
        grads_val = self.sess.run([grads], feed_dict={self.image_tensor:interpolated_images})

        grads_by_vals = np.multiply(interpolated_images, grads_val)
        weights = np.mean(grads_by_vals[0], axis=0)
        reduce_color_axis = np.mean(weights, axis=-1)
        truncated = np.maximum(reduce_color_axis, 0)
        normalized = truncated / np.amax(truncated)
        overlay = cv2.cvtColor(cv2.applyColorMap(np.uint8(255*normalized), cv2.COLORMAP_MAGMA), cv2.COLOR_BGR2RGB)

        #generate image
        raw_image = load_cropped_image(image_dir)
        return raw_image, overlay, overlay*0.6 + raw_image * 0.4

    def get_gradcam(self, image_dir, class_index = 0):
        img = load_cropped_image(image_dir)/255.0
        
        #get activations for batch
        one_hot = tf.sparse_to_dense(class_index, [2], 1.0)
        signal = tf.multiply(self.out_tensor, one_hot)
        loss = tf.reduce_mean(signal)
        grads = tf.gradients(loss, self.target_tensor)[0]
        acts, grads_val = self.sess.run([self.target_tensor, grads], feed_dict={self.image_tensor:[img]})
        acts = acts[0]
        weights = np.mean(grads_val[0], axis=(0,1))

        cam = np.zeros(acts.shape[0:2], dtype=np.float32)

        #weight averaging
        for i, w in enumerate(weights):
            cam += w*acts[:,:,i]
        
        #apply relu
        cam = np.maximum(cam, 0)
        #normalize
        cam = cam/np.max(cam)
        cam = cv2.resize(cam, (img.shape[0], img.shape[1]))

        overlay = cv2.cvtColor(cv2.applyColorMap(np.uint8(255*cam), cv2.COLORMAP_MAGMA), cv2.COLOR_BGR2RGB)
        raw_image = load_cropped_image(image_dir)
        return raw_image, overlay, overlay*0.6 + raw_image * 0.4
        
        #get activations for batch
        


    # def get_scores_partial_run_test(self, image_dir, class_index = 0, target_name='mixed9/concat:0'):
    #     img = load_img(image_dir)

    #     #compute grads
    #     one_hot = tf.sparse_to_dense(class_index, [1000], 1.0)
    #     signal = tf.multiply(self.out_tensor, one_hot)
    #     loss = tf.reduce_mean(signal)
    #     grads = tf.gradients(loss, self.target_tensor)[0]
        
    #     #compute scores
    #     output, grads_val = self.sess.run([self.target_tensor, grads], feed_dict={self.image_tensor:[img]})
    #     class_score = np.multiply(output, grads_val)[0]
    #     #compute second grads
    #     grads_val_2 = self.sess.run([grads], feed_dict={self.target_tensor:output})
    #     if (np.array_equal(grads_val, grads_val_2)):
    #         print("equal")
    #     else:
    #         print("not equal")
    #         print(np.max(grads_val))
    #         print(np.max(grads_val_2))
    #         print(np.min(grads_val))
    #         print(np.min(grads_val_2))
    #         print(np.max(grads_val - grads_val_2))
    #     #return class_score, np.max(class_score)
    
    # def get_preds_partial_run_test(self, image_dir, class_index = 0, target_name='mixed9/concat:0'):
    #     img = load_img(image_dir)
        
    #     #compute scores
    #     output= self.sess.run([self.target_tensor], feed_dict={self.image_tensor:[img]})
    #     preds = self.sess.run([self.out_tensor], feed_dict={self.target_tensor:output[0]})
    #     preds_2 = self.get_prediction(image_dir)
    #     print(decode_predictions(preds[0]))
    #     print(preds_2)
        