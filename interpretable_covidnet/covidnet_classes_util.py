

class CovidNetClassesUtil():
    
    def __init__(self):
        self.index_to_name = {0:'negative', 1:'positive'}
        self.name_to_index = {'negative':0, 'positive':1}
        self.index_to_code = {'negative':0, 'positive':1}
    
    def get_name_from_index(self, index):
        return self.index_to_name[index]
    
    def get_index_from_name(self, name):
        return self.name_to_index[name]
    
    def get_code_from_index(self, index):
        return self.index_to_code[index]
    
    def get_index_from_code(self, code):
        return self.code_to_index[code]

    def get_name_from_code(self, code):
        return self.index_to_name[self.code_to_index[code]]
    
    def get_code_from_name(self, name):
        return self.index_to_code[self.name_to_index[name]]